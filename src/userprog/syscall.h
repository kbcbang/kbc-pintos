#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

void syscall_init (void);

struct lock filesys_lock; // 
void exit(int status);
void syscall_init (void);
void check_address(void *addr);
void get_argument (void *esp, int *arg, int count);
int filesize(int fd);
int open (const char *file);

#endif /* userprog/syscall.h */
