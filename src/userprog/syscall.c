#include "userprog/syscall.h"
#include <stdio.h>
#include <stdlib.h>
#include <list.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "threads/palloc.h"
#include "threads/malloc.h"

static void syscall_handler (struct intr_frame *);


void halt(void);
void exit(int status);
bool create(const char *file, unsigned int initial_size);
bool remove(const char *file);
tid_t exec(const char *cmd_line);
int wait(tid_t tid);
int open(const char *open_filename);
int filesize(int fd);
int write(int fd, void *buffer, unsigned size);
int read(int fd, char *buffer, unsigned int size);
void seek(int fd, unsigned int position);
unsigned int tell(int fd);
void close(int fd);
void get_arg (void *esp, int *arg, int n);
void check_address(void *addr);
void
syscall_init (void) 
{
  lock_init(&filesys_lock);
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  int arg[4];
  void* esp = f->esp;
  int syscall_number;
  check_address(esp);

  syscall_number = *(int*)esp;

  switch (syscall_number )
    {
    case SYS_HALT:
      {
	halt(); 
	break;
      }
    case SYS_EXIT:
      {
	get_arg(esp, &arg[0], 1);
	exit(arg[0]);
	break;
      }
    case SYS_EXEC:
      {
	get_arg(esp, &arg[0], 1);
	check_address((void*)arg[0]);
	f->eax = exec((const char *) arg[0]); 
	break;
      }
    case SYS_WAIT:
      {
	get_arg(esp, &arg[0], 1);
	f->eax = wait((tid_t)arg[0]);
	break;
      }
    case SYS_CREATE:
      {
	get_arg(esp, &arg[0], 2);
	check_address((void*)arg[0]);
	f->eax = create((const char *)arg[0], (unsigned) arg[1]);
	break;
      }
    case SYS_REMOVE:
      {
	get_arg(esp, &arg[0], 1);
	check_address((void*)arg[0]);
	f->eax = remove((const char *) arg[0]);
	break;
      }
    case SYS_OPEN:
      {
	get_arg(esp, &arg[0], 1);
	check_address((void*)arg[0]);
	f->eax = open((const char *) arg[0]);
	break; 		
      }
    case SYS_FILESIZE:
      {
	get_arg(esp, &arg[0], 1);
	f->eax = filesize(arg[0]);
	break;
      }
    case SYS_READ:
      {
	get_arg(esp, &arg[0], 3);
	check_address((void*)arg[1]);
	f->eax = read(arg[0], (char *) arg[1], (unsigned) arg[2]);
	break;
      }
    case SYS_WRITE:
      { 
	get_arg(esp, &arg[0], 3);
	check_address((void*)arg[1]);
	f->eax = write(arg[0], (void *) arg[1],
	       (unsigned) arg[2]);
	break;
      }
    case SYS_SEEK:
      {
	get_arg(esp, &arg[0], 2);
	seek(arg[0], (unsigned) arg[1]);
	break;
      } 
    case SYS_TELL:
      { 
	get_arg(esp, &arg[0], 1);
	f->eax = tell(arg[0]);
	break;
      }
    case SYS_CLOSE:
      { 
	get_arg(esp, &arg[0], 1);
	close(arg[0]);
	break;
      }
    default      :
	thread_exit ();
    }
}
void
check_address(void *addr)
{
	if((unsigned)addr < 0x08048000 || (unsigned)(addr+3)> 0xc0000000)
	{
		exit(-1);
	}		
}
void get_arg (void *esp, int *arg, int n)
{
  int i;
  int *ptr;
  for (i = 0; i < n; i++)
    {
      ptr = (int *)esp + i +1;
      check_address(ptr);
      arg[i] = *ptr;
    }
}
void 
halt(void)
{
	shutdown_power_off();
}
void
exit(int status)
{
	struct thread *cur = thread_current();
	cur->exit_status = status;
	printf("%s: exit(%d)\n", cur->name, status);
	thread_exit();
}
bool
create(const char *file, unsigned initial_size)
{
	bool success = filesys_create(file, initial_size);
	return success;
}
bool
remove(const char *file)
{
	bool success = filesys_remove(file);
	return success;
}
tid_t
exec(const char* cmd_line)
{
	tid_t pid = process_execute(cmd_line);
	struct thread *cp = get_child_process(pid);
	if (cp == NULL)
		return -1;
	if (cp->is_load == 0) sema_down(&cp->load);
	if (cp->is_load == 1)
		return -1;
	
	return pid;
}
int
wait(tid_t tid)
{
	return process_wait(tid);
}
int
open(const char *file)
{
	
	struct file *of = filesys_open(file);
	if(!of)
	{
		
		return -1;
	}
	
	return process_add_file(of);
	

}
int 
filesize (int fd)
{
	
	struct file *of = process_get_file(fd);
	if(!of)
	{
		return -1;
	}
	return file_length(of);
	
}
int
read(int fd, char* buffer, unsigned size)
{
	int size_;
	lock_acquire(&filesys_lock);
	if(fd == 0)
	{
		unsigned int i;
		for( i=0; i<size; i++)
		{
			buffer[i]= (char)input_getc();
		}
		lock_release(&filesys_lock);	
		
		return size;	
	}
	else if(fd == 1)
	{
		lock_release(&filesys_lock);
		return -1;
	}
	struct file *read_file = process_get_file(fd);
	if(!read_file)
	{
		lock_release(&filesys_lock);
		return -1;	
	}
	
	size_= file_read(read_file,buffer,size);
	lock_release(&filesys_lock);
	return size_;


}
int
write(int fd, void* buffer, unsigned size)
{
	int write_size;
	lock_acquire(&filesys_lock);
		
	if(fd == 0)
	{
		lock_release(&filesys_lock);
		return -1;
	
	}
	else if( fd == 1)
	{
		lock_release(&filesys_lock);
		putbuf(buffer, size);
		return size;
	}

	struct file *write_file = process_get_file(fd); 
	if(!write_file)
	{
		lock_release(&filesys_lock);
		return -1;
	}

	write_size = file_write(write_file, buffer, size);
	lock_release(&filesys_lock);

	return write_size;
}
void 
seek (int fd, unsigned position)
{
	struct file *seek_file = process_get_file(fd);
	if(!seek_file)
	{
		return;
	}
	file_seek(seek_file, (off_t)position);
}

unsigned int
tell(int fd)
{ 
	struct file *tell_file = process_get_file(fd); 


	if(!tell_file)
	{
		return -1;
	}
	
	return file_tell(tell_file);
	
}
void
close(int fd)
{
	process_close_file(fd);
}

